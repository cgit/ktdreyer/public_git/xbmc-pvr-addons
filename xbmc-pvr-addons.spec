%global commitdate 20140126
%global commit d4cb4d5c0e7fc1a4b9a3b911c3053df178c36944
%global shortcommit %(c=%{commit}; echo ${c:0:7})

Name:		xbmc-pvr-addons
Version:	0
Release:	0.1.%{commitdate}git%{shortcommit}%{?dist}
Summary:	PVR addons for XBMC
Group: 		Applications/Multimedia
License:	GPLv3
URL:		https://github.com/opdenkamp/xbmc-pvr-addons
Source0:	xbmc-pvr-addons-%{shortcommit}-patched.tar.xz
Source1:	xbmc-pvr-addons-generate-tarball-xz.sh

BuildRequires:	autoconf
BuildRequires:	automake
BuildRequires:	boost-devel
BuildRequires:	libtool
BuildRequires:	mysql-devel
BuildRequires:	xbmc-devel
Requires:	xbmc

%description
PVR addons for XBMC media player

%prep
%setup -q -n pvr-addons

%build
./bootstrap
%configure --enable-addons-with-dependencies
make %{?_smp_mflags}

%install
make install DESTDIR=%{buildroot}

%files
%{_libdir}/pvr.*
%{_datarootdir}/pvr.*

%changelog
* Sun Jan 26 2014 Ken Dreyer <ktdreyer@ktdreyer.com> - 0-0.1
- Initial package (match version from xbmc 13.0 alpha11)
